package org.develnotes.org.customsearch;

import java.net.URL;
import java.util.List;

/**
 * 
 * Пример демонстрирующий использование Google Custom Search API.
 * develnotes.org 
 * 
 * @author Alex Dl.
 *
 */
public class Main {
	
	private static final String QUERY = "Привет Google";
	private static final int PAGE = 1;
	
	public static void main(String args[]){
		
		//выполняем поиск в Google с запросом первой страницы результатов поиска
		List<URL> result = CustomSearch.doGoogleSearch(QUERY, PAGE);
		
		System.out.println("Список ссылок из результатов поиска:");

		for (URL buff : result) {
			System.out.println(buff.toString());
		}
		
	}

}
