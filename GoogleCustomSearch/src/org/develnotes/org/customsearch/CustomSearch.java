package org.develnotes.org.customsearch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Пример демонстрирующий использование Google Custom Search API.
 * 
 * develnotes.org 
 * 
 * @author Alex Dl.
 *
 */
public class CustomSearch {

	// ключ API
	public final static String API_KEY = "YOUR_KEY";
	// ID поисковой системы
	public final static String SEARCH_ENGINE = "YOUR_SEARCH_ENGINE_KEY";

	private final static String ENCODING = "UTF-8";
	// полный URL для поискового запроса
	private final static String GET_URL = "https://www.googleapis.com/customsearch/v1?key="
			+ API_KEY + "&cx=" + SEARCH_ENGINE + "&q=";

	/**
	 * Выполнить поисковый запрос в Google
	 * 
	 * @param query
	 *            Строка запроса
	 * @param page
	 *            С какой страницы поиска необходимо получить результат (список
	 *            ссылок, максимальное число ссылок = 10). Номер страницы
	 *            начинается с 1.
	 * @return Список найденных ссылок из результатов поиска
	 */
	public static List<URL> doGoogleSearch(final String query, int page) {

		if (query == null || query.trim().isEmpty() || page < 1) {
			throw new IllegalArgumentException(
					"Ошибка: заданы некорректные аргументы.");
		}

		List<URL> result = new ArrayList<URL>();

		// заменяем все пробелы в поисковом запросе, если есть
		String searchQuery = query.replaceAll(" ", "+");

		System.out.println("Поиск начался.");

		try {
			// номер страцы результатов поиска Google
			String start = "&start=" + String.valueOf(page);
			// небходимо закодировать UTF-8 строку
			searchQuery = URLEncoder.encode(searchQuery, ENCODING);

			URL url = new URL(GET_URL + searchQuery + start + "&alt=json&");
			// выполняем GET запрос и получаем список ссылок
			// из результатов поиска
			result.addAll(getListOfUrlsFormGoogleResponse(url));

			System.out.println("Поиск завершен");
			
		} catch (Exception e) {
			System.err.println("Во время поиска произошла ошибка: "
					+ e.getMessage());
		}

		return result;
	}

	/**
	 * Выполнить GET запрос для поиска в Google
	 * @param url Сформированный URL запроса
	 * @return Список ссылок из результатов поиска
	 * @throws ProtocolException
	 * @throws IOException
	 */
	private static List<URL> getListOfUrlsFormGoogleResponse(final URL url)
			throws ProtocolException, IOException {

		List<URL> result = new ArrayList<URL>();

		// создаем соединение
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		// заголовоки GET запроса
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		
		System.out.println("Строка поискового запроса: " + url.toString());
		System.out.println("Ответ от сервера: ");

		// ответ от сервера
		// в ответе содержится различная информация, однако нам необходимо
		// получить
		// только URL

		String output;

		while ((output = br.readLine()) != null) {

			System.out.println(output);
			
			final String PARAM = "\"link\": \"";
			
			// берем из ответа только ссылку и добавляем ее в результат
			if (output.contains(PARAM)) {
				String link = output.substring(
						output.indexOf(PARAM) + (PARAM).length(),
						output.indexOf("\","));
				
				result.add(new URL(link));
			}
		}

		//закрываем сетевое соединение
		conn.disconnect();

		return result;
	}
}
