package org.develnotes.reporter;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

/**
 * Пример с сайта develnotes.org
 * @author Alex Dl.
 */
public class Main {

    public static void main(String [] args) {

        try {
            byte[] pdf = HTMLPrintUtil.printPdfReport("items_report.html", getTestData());
            FileUtils.writeByteArrayToFile(new File("src/main/resources/print/goods.pdf"), pdf);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Map<String, Object> getTestData() {

        Map<String, Object> data = new HashMap<>();
        List<Map<String,String>> table = new ArrayList<>();

        data.put("date", LocalDate.now().toString());

        Map<String, String> row = new HashMap<>();

        row.put("name", "iPhone 7");
        row.put("count", "100");
        row.put("price", "39000");
        row.put("available", "Есть на складе");
        table.add(row);

        row = new HashMap<>();
        row.put("name", "Samsung Galaxy");
        row.put("count", "10");
        row.put("price", "20000");
        row.put("available", "Есть на складе");
        table.add(row);

        row = new HashMap<>();
        row.put("name", "Наушники PANASONIC");
        row.put("count", "150");
        row.put("price", "2000");
        row.put("available", "Есть в магазине");
        table.add(row);

        data.put("goods-rows", table);

        return data;
    }

}
