package org.develnotes.service;

import java.util.ArrayList;
import java.util.List;

import org.develnotes.entity.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Сервис для работы с данными пользователей.
 * 
 * Этот класс представляет собой "заглушку", которая должна быть заменена на
 * реальную реализацию. При вызове метода {@link #loadUserByUsername(String)}
 * может быть получен пользователь с логином user
 * 
 * @author Alex Dl. www.develnotes.org
 *
 */
@Service
public class UserDetailsServiceStub implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		if ("user".equals(username)) {

			List<SimpleGrantedAuthority> auths = new ArrayList<SimpleGrantedAuthority>();
			auths.add(new SimpleGrantedAuthority("ROLE_USER"));
			
			return new User(username, "$2a$06$6rdrf2NymngVRlA8nkA9/OA5a2XfP2975VCo1Nb7UdiPX6xFqnyZ2", 
					 auths, "Россия",
					"Василий Васильевич");
		}
		throw new UsernameNotFoundException("User with name = " + username + " not found");
	}
}
