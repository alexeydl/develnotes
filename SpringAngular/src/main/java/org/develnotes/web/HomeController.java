package org.develnotes.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Контроллер домашней страницы
 * Перенаправляет пользователя на страницу index.html.
 * 
 * @author Alex Dl. www.develnotes.org
 *
 */
@Controller
public class HomeController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "redirect:/resources/index.html";
	}
}
