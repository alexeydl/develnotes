package org.develnotes.web;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.develnotes.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Контроллер аутентификации.
 * 
 * @author Alex Dl. www.develnotes.org
 *
 */
@RestController
@RequestMapping("/auth")
public class LoginController {

	private Logger log = LoggerFactory.getLogger(LoginController.class);

	/**
	 * Метод аутентификации/получения данных пользователя. GET
	 * запрос должен содержать заголовок аутентификации, с данными пользователя
	 * (логин и пароль). Если заголовок отсутствует или переданны неверные
	 * данные, то метод вернет HTTP статус 403.
	 * 
	 * @param user Данные пользователя, автоматически предоставляются Spring
	 *             Security, если в заголовке GET запроса были предоставлены
	 *             корректные данные пользователя.
	 * @return Данные пользователя или null.
	 */
	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json")
	public User getUser(Principal principial) {
		
		if (principial != null) {
			
			log.info("Got user info for login = " + principial.getName());
			
			if (principial instanceof AbstractAuthenticationToken){
				return (User) ((AbstractAuthenticationToken) principial).getPrincipal();
			} 
		}
		
		return null;
		
	}
	
	/**
	 * Выйти из системы. 
	 * @param rq {@link javax.servlet.http.HttpServletRequest}
	 * @param rs {@link javax.servlet.http.HttpServletResponse}
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public void logout(HttpServletRequest rq, HttpServletResponse rs) {
		
	    SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
	    securityContextLogoutHandler.logout(rq, rs, null);
		
	}
	
}
