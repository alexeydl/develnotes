# README #
##Примеры кода  http://www.develnotes.org

##GoogleCustomSearch - Код примера использования Google Custom Search API. http://www.develnotes.org/article143760
##PdfReport - Печать PDF на основе HTML5 шаблона. http://www.develnotes.org/article150938
##SpringAngular - Исходный код примера аутентификации с помощью Spring Security 4 в приложении AngularJS. http://www.develnotes.org/article146289
##SpringSecurityTest - Проект для Spring Tool Suite, демонстрирующий использование Spring Security. http://www.develnotes.org/article70619
##SpringSecurityTestMysql - Исходный код веб-приложения JavaEE, в котором используется авторизация с помощью Spring Security + MySQL. http://www.develnotes.org/article111555
##TicTacToe - Исходники игры в крестики-нолики на поле размером 5x5.  http://www.develnotes.org/article60626